create materialized view  "JIRA_SPRINT_HISTORY_MVW" ("SPRINT_NAME", "TICKETS_TOTAL", "TICKETS_AT_START", "TICKETS_ADDED", "TICKETS_REMOVED", "TICKETS_DONE", "TICKETS_NOTDONE", "TICKETS_PRODFIND", "TICKETS_UATFIND", "TICKETS_NULL", "POINTS_AT_START", "VELOCITY", "POINTS_ADDED", "POINTS_NOTFINISHED", "POINTS_REMOVED")
as
with sprints
as( select spr.id
    ,      spr.name
    ,      spr.name                 sprint_name
    ,      case
             when spr.started = 1
             and  spr.closed  = 1
             then 'past'
             when spr.started = 1
             and  spr.closed  = 0
             then 'active'
             when spr.started = 0
             then 'future'
           end                      sprint_status
    ,      spr.started
    ,      spr.closed
    ,      spr.start_date
    ,      spr.end_date
    ,      nvl(spr.complete_date, sysdate) complete_date
    --,      cast(( from_tz( cast( spr.start_date    as timestamp),'+00:00') at local ) as date) start_date
    --,      cast(( from_tz( cast( spr.end_date      as timestamp),'+00:00') at local ) as date) end_date
    --,      cast(( from_tz( cast( spr.complete_date as timestamp),'+00:00') at local ) as date) complete_date
    from   jira_rep_sprints spr
    where  1=1
    --and    substr( name, 1, instr(name,' 20')-1) = :P2_SQUAD
    and    name                                like '% 20__ wk%__/__%'
    --and    name                                = :P6_SPRINT
    and    spr.started                         = 1
    and    spr.start_date >= trunc(sysdate) - (7*14+2)
  )
,   his_issues
as( select spr.sprint_name
    ,      spr.sprint_status
    ,      spr.start_date           sprint_start
    ,      spr.end_date             sprint_end
    ,      spr.complete_date        sprint_complete
    ,      iss.project
    ,      iss.issuetype
    ,      iss.key
    ,      iss.status
    ,      iss.created
    ,      nvl( ( select distinct
                         trim(substr(first_value(h2.newval) over( order by h2.date_of_change desc), nvl(instr(first_value(h2.newval) over( order by h2.date_of_change desc),',',-1)+1,0))) sprints
                  from   jira_his_issue_sprints h2
                  where  h2.issuekey        = his.issuekey
                  and    h2.date_of_change >= his.date_of_change
                  and    h2.date_of_change <  spr.complete_date
                )
              , sprint_name
              )                     ending_sprint
    ,      iss.updated
    ,      iss.fixversions
    ,      iss.technology
    ,      iss.storypoints
    ,      iss.sprints
    ,      his.date_of_change date_in_sprint
    from   sprints         spr
    ,      jira_his_issue_sprints  his
    ,      jira_issues_all iss
    where  his.newval         like '%'||spr.name
    and    iss.key               = his.issuekey
    and    his.date_of_change   <  spr.complete_date
    and    iss.issuetype   not in( 'Approval', 'Sub-task')
    and    his.date_of_change   = ( select max(h2.date_of_change)
                                    from   jira_his_issue_sprints h2
                                    where  h2.issuekey       = his.issuekey
                                    and    h2.date_of_change < spr.complete_date
                                    and    h2.newval         like '%'||spr.name
                                  )
    and    not exists( select 1
                       from   jira_his_issue_sprints h2
                       where  h2.issuekey = his.issuekey
                       and    h2.date_of_change < spr.start_date
                       and    h2.date_of_change > his.date_of_change
                       and    (  h2.newval not like '%'||spr.name
                              or h2.newval is null
                              )
                     )
  )
,   createdissues
as( select spr.sprint_name
    ,      iss.issuetype
    ,      iss.key
    ,      iss.status
    ,      iss.sprints
    ,      spr.start_date           sprint_start
    ,      spr.complete_date        sprint_complete
    ,      iss.created              date_in_sprint
    ,      nvl( ( select distinct
                         trim(substr(first_value(h2.newval) over( order by date_of_change desc), nvl(instr(first_value(h2.newval) over( order by date_of_change desc),',',-1)+1,0))) sprints
                  from   jira_his_issue_sprints h2
                  where  h2.issuekey        = iss.key
                  and    h2.date_of_change >= iss.created
                  and    h2.date_of_change <  spr.complete_date
                )
              , spr.name
              ) ending_sprint
    ,      storypoints
    from   sprints         spr
    ,      jira_issues_all iss
    where  iss.sprints   like '%'||spr.name||'%'
    and    iss.issuetype not in( 'Approval', 'Sub-task')
    and    iss.created   < spr.complete_date
    and    not exists( select 1
                       from   his_issues his
                       where  his.key         = iss.key
                       and    his.sprint_name = spr.sprint_name
                     )
  )
,    total_sprint_view
as( select issuetype
    ,      key
    ,      status
    ,      date_in_sprint
    ,      ending_sprint
    ,      sprint_name
    ,      sprint_start
    ,      sprint_complete
    ,      sprints
    ,      storypoints
    from   his_issues    his
    union all
    select issuetype
    ,      key
    ,      status
    ,      date_in_sprint
    ,      ending_sprint
    ,      sprint_name
    ,      sprint_start
    ,      sprint_complete
    ,      sprints
    ,      storypoints
    from   createdissues cis
  )
,    sprint_velocity_data
as( 
select issuetype
    ,      key
    ,      status
    ,      date_in_sprint
    ,      ending_sprint
    ,      sprint_name
    ,      sprint_start
    ,      sprint_complete
    ,      sprints
    ,      case
             when date_in_sprint <= sprint_start
             then 1
             else 0
           end      in_at_start
    ,      case
             when ending_sprint != sprint_name
             then 0
             else 1
           end      in_at_end
    ,      coalesce( ( select distinct
                              to_number(first_value(his.newval) over( partition by his.issuekey order by his.date_of_change desc))
                       from   jira_his_issue_points his
                       where  his.issuekey        = key
                       and    his.date_of_change <= sprint_start
                     )
                   , ( select distinct
                              to_number(first_value(his.oldval) over( partition by his.issuekey order by his.date_of_change))
                       from   jira_his_issue_points his
                       where  his.issuekey       = key
                       and    his.date_of_change > sprint_start
                     )
                   , case when date_in_sprint <= sprint_start then storypoints end
                   , 0
                   )                      points_at_start
    ,      coalesce( ( select distinct
                              to_number(first_value(his.newval) over( partition by his.issuekey order by his.date_of_change desc))
                       from   jira_his_issue_points his
                       where  his.issuekey        = key
                       and    his.date_of_change <= sprint_complete
                     )
                   , ( select distinct
                              to_number(first_value(his.oldval) over( partition by his.issuekey order by his.date_of_change))
                       from   jira_his_issue_points his
                       where  his.issuekey       = key
                       and    his.date_of_change > sprint_complete
                     )
                   , case when date_in_sprint <= sprint_start then storypoints end
                   , 0
                   )                      points_at_end
    ,      coalesce( ( select distinct
                              first_value(his.newval) over( partition by his.issuekey, his.field order by his.date_of_change desc)
                       from   jira_his_issue_status his
                       where  his.issuekey        = key
                       and    his.date_of_change <= sprint_complete
                     )
                   , ( select distinct
                              first_value(his.oldval) over( partition by his.issuekey, his.field order by his.date_of_change)
                       from   jira_his_issue_status his
                       where  his.issuekey       = key
                       and    his.date_of_change > sprint_complete
                     )
                   , status
                   )                      status_at_end
    from   total_sprint_view
  )
select sprint_name
,      count(1)    total_tickets
,      sum( case when in_at_start = 1 then 1 else 0 end ) tickets_at_start
,      sum( case when in_at_start = 0 then 1 else 0 end ) tickets_added
,      sum( case when in_at_end   = 0 then 1 else 0 end ) tickets_removed
,      sum( case when in_at_end = 1 and status_at_end  = 'Done' then 1 else 0 end ) tickets_done
,      sum( case when in_at_end = 1 and status_at_end != 'Done' then 1 else 0 end ) tickets_notdone
,      sum( case when issuetype = 'Prod Finding' then 1 else 0 end) tickets_prodfind
,      sum( case when issuetype = 'UAT Finding'  then 1 else 0 end) tickets_uatfind
,      sum( case when points_at_end = 0 then 1 else 0 end) tickets_null
,      sum( case when in_at_start = 1 then points_at_start else 0 end) points_at_start
,      sum( case 
              when status_at_end = 'Done' 
              and  in_at_end     = 1
              then points_at_end 
              else 0 
            end) velocity
,      sum( case 
              when points_at_end > points_at_start
              then points_at_end - points_at_start
              when in_at_start = 0
              then points_at_end
              else 0
            end
          ) points_added
,      sum( case when in_at_end = 1 and status != 'Done' then points_at_end else 0 end) points_notfinished
,      sum( case
              when points_at_start > points_at_end
              then points_at_start - points_at_end
              when in_at_start = 1
              and  in_at_end   = 0
              then points_at_start
              else 0
            end
          ) points_removed
from   sprint_velocity_data
group by sprint_name, sprint_start
order by sprint_start desc
